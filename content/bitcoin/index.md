+++
categories = ["bitcoin"]
comments = false
date = "2016-10-02T22:55:05-04:00"
draft = false
slug = ""
tags = ["bitcoin"]
title = "Bitcoin Resources"

showpagemeta = false
+++


[Jameson Lopp's Bitcoin Resource Guide](https://lopp.net/bitcoin.html)

[Andreas Antonopoulos Youtube channel](https://www.youtube.com/aantonop)

[Daniel Jeffries Eight Simple Rules for Protecting Your Cryptocurrency](https://hackernoon.com/eight-simple-rules-for-protecting-your-cryptocurrency-5cdddc9f674d)

[Pierre Rochard's "Understanding the Technical Side of Bitcoin"](https://medium.com/@pierre_rochard/understanding-the-technical-side-of-bitcoin-2c212dd65c09)

[The Positive Externalities of Bitcoin Mining, a summary](https://medium.com/nodeblockchain/bitcoin-energy-b230a9d7dd5d)

[Running a full node](https://bitcoin.org/en/full-node)

[Run a Bitcoin full node on a Raspberry Pi](https://github.com/Stadicus/guides/blob/master/raspibolt/README.md)

[Run Lightning on a Raspberry Pi](http://velascommerce.com/lightning-pi)

[Run a Bitcoin full node on a ___](alexandrag url)

[Run a Bitcoin full node on Linux](https://hackernoon.com/a-complete-beginners-guide-to-installing-a-bitcoin-full-node-on-linux-2018-edition-cb8e384479ea)

[How to create an online store and accept bitcoin, a step-by-step guide](https://bitcoinshirt.co/how-to-create-store-accept-bitcoin/)

[HODL](https://bitcointalk.org/index.php?topic=375643.0)